package com.company;

import com.company.model.Language;
import com.company.model.Staff;
import com.company.service.LanguageService;
import com.company.service.StaffService;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws SQLException {

        StaffService staffService = new StaffService();
        LanguageService languageService = new LanguageService();
        int paramCode;
        String paramName;
        String nameLanguage;
        int id;
        while (true) {
            System.out.println("---------------Menu---------------");
            System.out.println("1 - Hiển Thị Danh Sách Nhân Viên!");
            System.out.println("2 - Thêm Mới Nhân Viên!");
            System.out.println("3 - Xoá Nhân Viên!");
            System.out.println("4 - Sửa Nhân Viên!");
            System.out.println("5 - join!");
            System.out.println("6 - Thêm Danh Sách Ngôn Ngữ!");
            System.out.println("7 - Hiển Thị Danh Sách Ngôn Ngữ!");
            System.out.println("8 - Thêm Mới Nhân Viên + Ngôn Ngữ!");
            System.out.println("9 - Chỉnh sửa Nhân Viên + Ngôn Ngữ!");
            System.out.println("----------------------------------");
            System.out.print("Nhập tại đây: ");
            Scanner sc = new Scanner(System.in);
            if (sc.hasNextInt()) {
                int n = sc.nextInt();
                switch (n) {
                    case 1:
                        List<Staff> staffList = staffService.layDanhSach();
                        System.out.println("Đây là Danh Sách Nhân Viên: ");
                        for (Staff staff: staffList) {
                            System.out.println(staff);
                        }
                        break;
                    case 2:
                        System.out.print("Nhập mã NV: ");
                        paramCode = sc.nextInt();
                        sc.nextLine();
                        System.out.print("Nhập tên NV: ");
                        paramName= sc.nextLine();
                        Staff staff = new Staff();
                        staff.setName(paramName);
                        staff.setCode(paramCode);
                        staffService.themMoi(staff);
                        break;
                    case 3:
                        System.out.print("Nhập mã NV cần xoá: ");
                        if (sc.hasNextInt()) {
                            paramCode = sc.nextInt();
                            sc.nextLine();
                            staffService.xoa(paramCode);
                        } else {
                            System.out.println("nhập k đúng!");
                            sc.next();
                        }
                        break;
                    case 4:
                        System.out.print("Nhập mã NV cần sửa: ");
                        paramCode = sc.nextInt();
                        sc.nextLine();
                        System.out.print("Nhập tên NV cần sửa: ");
                        paramName = sc.nextLine();
                        Staff staff1 = new Staff();
                        staff1.setCode(paramCode);
                        staff1.setName(paramName );
                        staffService.capNhat(staff1);
                        break;
                    case 5:
                        List<Language> languages = languageService.join();
                        System.out.println("Đây là Danh Sách Nhân Viên: ");
                        for (Language language: languages) {
                            System.out.println(language);
                        }
                        break;
                    case 6:
                        System.out.print("Nhập id: ");
                        paramCode = sc.nextInt();
                        sc.nextLine();
                        System.out.print("Nhập tên Ngôn Ngữ: ");
                        paramName= sc.nextLine();
                        Language language = new Language();
                        language.setName(paramName);
                        language.setCode(paramCode);
                        languageService.themMoi(language);
                        break;
                    case 7:
                        List<Language> languages1 = languageService.layDanhSach();
                        System.out.println("Đây là Danh Sách Ngôn Ngữ: ");
                        for (Language l : languages1) {
                            System.out.println("Language{" +
                                    "id=" + l.getId() +
                                    ", nameLanguage='" + l.getNameLanguage() + '\'' +
                                    '}');
                        }
                        break;
                    case 8:
                        System.out.print("Nhập id Ngôn Ngữ: ");
                        id = sc.nextInt();
                        sc.nextLine();
                        System.out.print("Nhập mã NV: ");
                        paramCode = sc.nextInt();
                        sc.nextLine();
                        System.out.print("Nhập Ngôn Ngữ mới: ");
                        nameLanguage= sc.nextLine();
                        Language language1 = new Language();
                        if(language1.getId()!= id){
                            language1.setId(id);
                            language1.setNameLanguage(nameLanguage);
                            language1.setCode(paramCode);
                            languageService.themMoi(language1);
                        } else
                            System.out.println("Lỗi rồi");
                        break;
                    case  9:
                        System.out.print("Nhập mã Language cần sửa: ");
                        id = sc.nextInt();
                        sc.nextLine();
                        System.out.print("Nhập Tên Language cần sửa: ");
                        nameLanguage = sc.nextLine();
                        System.out.print("Nhập Mã Staff cần sửa: ");
                        paramCode = sc.nextInt();
                        sc.nextLine();
                        Language language2 = new Language();
                        language2.setCode(id);
                        language2.setNameLanguage(nameLanguage);
                        language2.setCode(paramCode );
                        languageService.capNhat(language2);
                        break;
                    default:
                        System.out.println("----------------------------------");
                        System.out.println("Không có chương trình như mong muốn - chương trình kết thúc");
                        System.exit(0);
                        break;
                }
            } else {
                System.out.println(sc.nextLine() + " - Không phải kiểu số, nhập lại! ");
            }
        }
    }
}
