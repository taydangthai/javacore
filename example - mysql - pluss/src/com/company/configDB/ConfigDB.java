package com.company.configDB;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.*;
import java.util.logging.*;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;


public class ConfigDB {

	public static Logger logger =  Logger.getLogger(ConfigDB.class.getName());
	private static final String className = "com.mysql.cj.jdbc.Driver";
	private static final String url = "jdbc:mysql://localhost:3306/staffmysql?autoReconnect=true&useSSL=false";
	private static final String user = "taystp";
	private static final String pass = "3009961";
	private static final String table = "staff";
	private static Connection connection;
	private static Handler consoleHandler = null;
	private static Handler fileHandler  = null;
	public static Connection Connect() throws SQLException
	{
		try
		{
			consoleHandler = new ConsoleHandler();
			fileHandler  = new FileHandler("./tay.log");
			//Assigning handlers to LOGGER object
			logger.addHandler(consoleHandler);
			logger.addHandler(fileHandler);
			//Setting levels to handlers and LOGGER
			consoleHandler.setLevel(Level.ALL);
			fileHandler.setLevel(Level.ALL);
			logger.setLevel(Level.ALL);
			//Console handler removed
			logger.removeHandler(consoleHandler);

			Class.forName(className);
			connection = DriverManager.getConnection(url,user,pass);
			logger.log(Level.FINE, "Kết nối thành công");
//			logger.config("Configuration done.");
		} catch (ClassNotFoundException ex) {
			logger.info(ex.getMessage() + "- Thất cmn Bại khi kết nối rồi");
		} catch (SQLException e){
			logger.warning(e.getMessage() + "\nConfig tới Mysql sai thông tin, hãy kiểm tra");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return connection;
	}
}
