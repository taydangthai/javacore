package com.company.model;
import java.util.List;
import java.util.Set;


public class Staff {

	private int code;
	private String name;
	private List<Language> languages;

	public Staff() {
		
	}

	public Staff(int code, String name, List<Language> languages) {
		this.code = code;
		this.name = name;
		this.languages = languages;
	}

	public Staff(int code, String name) {
		super();
		this.code = code;
		this.name = name;
	}


	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	@Override
	public String toString() {
		return "Staff{" +
				"code=" + code +
				", name='" + name + '\'' +
				'}';
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
