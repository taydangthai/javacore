package com.company.model;

public class Language {
    private int id;
    private String nameLanguage;
    private int code;
    private String name;

    public Language() {
    }

    @Override
    public String toString() {
        return "Language{" +
                "id=" + id +
                ", nameLanguage='" + nameLanguage + '\'' +
                ", code=" + code +
                ", nameStaff='" + name +'\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Language(int id, String nameLanguage, int code) {
        this.id = id;
        this.nameLanguage = nameLanguage;
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameLanguage() {
        return nameLanguage;
    }

    public void setNameLanguage(String nameLanguage) {
        this.nameLanguage = nameLanguage;
    }
}
