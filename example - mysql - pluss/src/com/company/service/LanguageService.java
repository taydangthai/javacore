package com.company.service;

import com.company.configDB.ConfigDB;
import com.company.model.Language;
import com.company.model.Staff;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LanguageService {

    Logger logger = Logger.getLogger(Language.class.getName());

    private Connection conn = null;
    public List<Language> join() throws SQLException {

        try {
            //goi ham ket noi toi mysql
            conn = ConfigDB.Connect();
            Statement statement = conn.createStatement();

            //Thực hiện và trả về kết quả
            ResultSet resultSet = statement.executeQuery("" +
                    "select s.name, s.code, l.nameLanguage, l.id from staffmysql.staff s join staffmysql.language l on s.code = l.code");

            List<Language> languages = new ArrayList<>();
            //Đọc từng dòng thông tin
            while (resultSet.next()) {
                //Khởi tạo đối tượng
                Language language = new Language();
                language.setId(resultSet.getInt("id"));
                language.setName(resultSet.getString("name"));
                languages.add(language);
                language.setCode(resultSet.getInt("code"));
                language.setNameLanguage(resultSet.getString("nameLanguage"));
            }
            return languages;

        } catch (SQLException ex) {
            System.out.println("Có lỗi xảy ra trong quá trình làm việc với mysql. "
                    + "Chi tiết: " + ex.getMessage());
            logger.info(ex.getMessage());
        } finally {
            try {
                //Đóng kết nối
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(StaffService.class.getName()).log(Level.SEVERE, null, ex);
                logger.info(ex.getMessage());
            }
        }

        return null;
    }
    public boolean themMoi(Language language) {
        try {
            conn = ConfigDB.Connect();

            String strInsert = "Insert into language(code, nameLanguage, id) values(?,?,?)";
            PreparedStatement comm = conn.prepareStatement(strInsert);

            //Gán giá trị cho các tham số
            comm.setInt(1, language.getCode());
            comm.setString(2, language.getNameLanguage());
            comm.setInt(3, language.getId());

            return comm.executeUpdate()>0;

        } catch (SQLException ex) {
            System.err.println("Có lỗi không thêm được thông tin. Chi tiết: " + ex.getMessage());;
        } finally {
            try {
                //Đóng kết nối
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(StaffService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return false;
    }
    public List<Language> layDanhSach() throws SQLException {
        //Khai báo 1 danh sách
        List<Language> languages = new ArrayList();

        try {
            //goi ham ket noi toi mysql
            conn = ConfigDB.Connect();

            //Khai báo 1 công việc
            String strSQL = "Select id, nameLanguage"+" from language";

            Statement comm = conn.createStatement();

            //Thực hiện và trả về kết quả
            ResultSet rs = comm.executeQuery(strSQL);

            while (rs.next()) {
                //Khởi tạo đối tượng
                Language language = new Language();
                //Gán giá trị cho các thuộc tính
                language.setId(rs.getInt("id"));
                language.setNameLanguage(rs.getString("nameLanguage"));

                //Thêm vào danh sách

                languages.add(language);
            }

        } catch (SQLException ex) {
            System.out.println("Có lỗi xảy ra trong quá trình làm việc với mysql. "
                    + "Chi tiết: " + ex.getMessage());
        } finally {
            try {
                //Đóng kết nối
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(StaffService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return languages;
    }
    //update language set nameLanguage = "python" , code = 1 where id = 1;
    public boolean capNhat(Language language) {

        try {
            //Khởi tạo kết nối
            conn = ConfigDB.Connect();

            //Tạo công việc
            String strUpdate = "update language set nameLanguage = ? , code = ? where id = ?";
            PreparedStatement comm = conn.prepareStatement(strUpdate);

            //Gán giá trị cho các tham số

            comm.setString(1, language.getNameLanguage());
            comm.setInt(2, language.getCode());
            comm.setInt(3, language.getId());

            //Thực hiện công việc

            return comm.executeUpdate()>0;

        } catch (SQLException ex) {
            Logger.getLogger(StaffService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                //Đóng kết nối
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(StaffService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return false;
    }
}
