package quadraticequation;

import java.util.Scanner;

public class Main {

	private double a, b, c, x1, x2;

	public double nhapA(Scanner sc) {
		System.out.println("nhap a");
		do {
			if (sc.hasNextDouble()) {
				this.a = sc.nextDouble();
				return this.a;

			} else {
				System.out.println("Khong phai so, moi nhap lai :" + sc.next());
			}

		} while (true);

	}

	public double nhapB(Scanner sc) {
		System.out.println("nhap b");
		do {
			if (sc.hasNextDouble()) {
				this.b = sc.nextDouble();
				return this.b;

			} else {
				System.out.println("Khong phai so, moi nhap lai :" + sc.next());
			}
		} while (true);
	}

	public double nhapC(Scanner sc) {
		System.out.println("nhap c");

		do {
			if (sc.hasNextDouble()) {
				this.c = sc.nextDouble();
				return this.c;

			} else {
				System.out.println("Khong phai so, moi nhap lai :" + sc.next());
			}

		} while (true);

	}

	public void hien() {
		System.out.println("Số a đã nhập: " + this.a);
		System.out.println("Số b đã nhập: " + this.b);
		System.out.println("Số c đã nhập: " + this.c);
		System.out.println(
				"Phương trình bậc hai bạn vừa nhập có dạng: " + this.a + "x^2 + " + this.b + "x + " + this.c + " = 0");
	}

	public double tinhDelta() {
		double delta = ((this.b * this.b) - 4 * (this.a * this.c));
		return delta;
	}

	public void resoul() {
		if (tinhDelta() < 0) {
			System.out.println("Phương trình vô nghiệm");
			return;
		}
		if (tinhDelta() == 0) {
			this.x1 = -this.b / (2 * this.a);
			System.out.println("Phương trinh có 1 nghiệm là x1 = x2 = " + this.x1);
			return;
		} else {
			this.x1 = (-b + Math.sqrt(tinhDelta())) / (2 * this.a);
			this.x2 = (-b - Math.sqrt(tinhDelta())) / (2 * this.a);
			System.out.println("Phương trình có 2 nghiệm x1 = " + this.x1 + " và x2 = " + this.x2);
			return;
		}
	}

	public static void main(String[] args) {

		do {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Chọn chương trình, hãy chọn số: ");
			System.out.println("1 - giải phương trình bậc 1");
			System.out.println("2 - chưa có mẹ gì hết");
			int n = scanner.nextInt();

			switch (n) {
			case 1:
				System.out.println("đây là bài toán giải phương trình bậc 2: ");
				Main main = new Main();
				main.nhapA(scanner);
				main.nhapB(scanner);
				main.nhapC(scanner);
				main.hien();
				main.resoul();
				break;
			case 2:
				System.out.println("Đ** có gì hết ");
			default:
				System.out.println("Không có chương trình như mong muốn");
				break;
			}
			System.out.println("Muốn chạy lại không? (c/k)");
			String line = new Scanner(System.in).nextLine();
			if (line.equalsIgnoreCase("k"))
				break;
			if (line.equalsIgnoreCase("c"))
				continue;
			if (line != "k" || line != "c") {
				line = new Scanner(System.in).nextLine();
				System.out.println("chọn lại chương trình");
			}
		} while (true);
		System.out.println("ok - chương trình đã tắt");
	}
}
