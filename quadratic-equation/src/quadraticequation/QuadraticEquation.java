package quadraticequation;

import java.util.Scanner;

public class QuadraticEquation {

	private double a, b, c, x1, x2;

	public double nhapA(Scanner sc) {
		System.out.print("nhập a: ");
		do {
			if (sc.hasNextDouble()) {
				this.a = sc.nextDouble();
				return this.a;

			} else {
				System.out.println("Không phải số, nhập lại :" + sc.next());
			}

		} while (true);

	}

	public double nhapB(Scanner sc) {
		System.out.print("nhập b: ");
		do {
			if (sc.hasNextDouble()) {
				this.b = sc.nextDouble();
				return this.b;

			} else {
				System.out.println("Không phải số, nhập lại :" + sc.next());
			}
		} while (true);
	}

	public double nhapC(Scanner sc) {
		System.out.print("nhập c: ");

		do {
			if (sc.hasNextDouble()) {
				this.c = sc.nextDouble();
				return this.c;

			} else {
				System.out.println("Không phải số, nhập lại :" + sc.next());
			}

		} while (true);

	}

	public void hien() {
		System.out.println("Số a đã nhập: " + this.a);
		System.out.println("Số b đã nhập: " + this.b);
		System.out.println("Số c đã nhập: " + this.c);
		System.out.println(
				"Phương trình bậc hai bạn vừa nhập có dạng: " + this.a + "x^2 + " + this.b + "x + " + this.c + " = 0");
	}

	public double tinhDelta() {
		double delta = ((this.b * this.b) - (4 * (this.a * this.c)));
		return delta;
	}

	public void result() {
		if (this.a == 0 && this.b == 0 && this.c == 0) {
			System.out.println("Phương trình vô số nghiệm!");
			return;
		}
		if (this.a == 0 && this.b == 0) {
			System.out.println("Phương trình vô nghiệm!");
			return;
		}
		if (this.a == 0 && this.b != 0 && this.c != 0) {
			System.out.println("Phương trình có nghiệm là: " + (-this.c / this.b));
			return;
		}
		if (this.a != 0) {
			if (tinhDelta() < 0) {
				System.out.println("Phương trình vô nghiệm~~");
				return;
			}
			if (tinhDelta() == 0) {
				this.x1 = (double) (-this.b / 2 * (this.a));
				System.out.println("Phương trinh có 1 nghiệm kép x1 = x2 =  " + this.x1);
				return;
			}
			if (tinhDelta() > 0) {
				this.x1 = (double) (-this.b + Math.sqrt(tinhDelta())) / (2 * this.a);
				this.x2 = (double) (-this.b - Math.sqrt(tinhDelta())) / (2 * this.a);
				System.out.println("Phương trình có 2 nghiệm x1 = " + this.x1 + " và x2 =  " + this.x2);
				return;
			}
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		do {
			
			System.out.println("Chọn chương trình, hãy chọn số: ");
			System.out.println("1 - giải phương trình bậc 2");
			System.out.println("2 - chưa có *** gì hết");
			int n = scanner.nextInt();

			switch (n) {
			case 1:
				System.out.println("đây là bài toán giải phương trình bậc 2: ");
				QuadraticEquation q = new QuadraticEquation();
				q.nhapA(scanner);
				q.nhapB(scanner);
				q.nhapC(scanner);
				q.hien();
				q.result();
				break;
			case 2:
				System.out.println("Đ** có gì hết ");
			default:
				System.out.println("Không có chương trình như mong muốn");
				break;
			}
		} while (true);
	}
}
