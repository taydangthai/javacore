package personnel;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public List<Staff> listStaff = new ArrayList<Staff>();
	public Staff staff;
	public Scanner sc = new Scanner(System.in);

	public void createStaff() {
		int n;
		System.out.print("Nhập số lượng Nhân Viên: ");
		do {
			if (sc.hasNextInt()) {
				n = sc.nextInt();
				for (int i = 0; i < n; i++) {
					System.out.println("nhân viên thứ " + (i + 1));
					staff = new Staff();
					staff.createStaff(sc);
					listStaff.add(staff);
					
				}
				System.out.println("Thêm thành công!");
				return;
			} else {
				System.out.print(sc.next() + " K đúng kiểu - nhập lại đy: ");
				/*
				 * System.out.println("\nK đúng Y/c - nhập lại đy: "); return;
				 */
			}
		} while (true);
	}

	public void displayStaff() {
		if (listStaff.isEmpty()) {
			System.out.println("danh sách trống");
		} else {
			/*
			 * for (int i = 0; i < listStaff.size(); i++) {
			 * System.out.println(listStaff.get(i)); }
			 */
			for (Staff staff : listStaff) {
				System.out.println("Nhân viên " + listStaff.size() + " : " + staff.toString());
			}
		}
	}

	public void removeStaff(int code) {
		if (listStaff.isEmpty()) {
			System.out.println("danh sách trống");
		} else {
			for (Staff staff : listStaff) {
				if (staff.getCode() == code) {
					listStaff.remove(staff);
					System.out.println("đã xoá " + code);
				} else {
					System.out.println("không tìm thấy");
				}
				return;
			}
		}
	}

	public void updateStaff(int code, String newName) {
		if (listStaff.isEmpty()) {
			System.out.println("rỗng");
		} else {
			for (Staff staff : listStaff) {
				if (staff.getCode() == code) { // && staff.getName().equals(newName)
					staff.setCode(code);
					staff.setName(newName);
					System.out.println("Đã sửa nhân viên có mã: " + code);
				} else {
					System.out.println("không tìm thấy");
				}
				return;
			}
			
		}

	}

	public void findStarr(String name) {

	}

	public static void main(String[] args) {

		Main main = new Main();

		while (true) {
			System.out.println("---------------Menu---------------");
			System.out.println("1 - Hiển Thị Danh Sách Nhân Viên!");
			System.out.println("2 - Thêm Mới Nhân Viên!");
			System.out.println("3 - Xoá Nhân Viên!");
			System.out.println("4 - Sửa Nhân Viên!");
			System.out.println("----------------------------------");
			System.out.print("Nhập tại đây: ");
			Scanner sc = new Scanner(System.in);
			if (sc.hasNextInt()) {
				int n = sc.nextInt();
				switch (n) {
				case 1:
					main.displayStaff();
					break;
				case 2:
					main.createStaff();
					break;
				case 3:
					System.out.print("Nhập mã NV cần xoá: ");
					if (sc.hasNextInt()) {
						int code = sc.nextInt();
//						sc.nextLine();
						main.removeStaff(code);

					} else {
						System.out.println("nhập k đúng!");
						sc.next();
//						return;
					}
					break;
				case 4:
					System.out.print("Nhập mã NV cần sửa: ");
					int code1 = sc.nextInt();
					sc.nextLine();
					System.out.print("Nhập tên NV cần sửa: ");
					String name = sc.nextLine();
					main.updateStaff(code1, name);
					break;
				case 5:
					break;
				case 6:
					break;
				default:
					System.out.println("----------------------------------");
					System.out.println("Không có chương trình như mong muốn - chương trình kết thúc");
					System.exit(0);
					break;
				}
			} else {
				System.out.println(sc.nextLine() + " - Không phải kiểu số, mời nhập lại! ");
			}
		}
	}
}
