package model;

import java.util.Scanner;

public class Staff {

	private int code;
	private String name;

	public Staff() {
		
	}

	public Staff(int code, String name) {
		super();
		this.code = code;
		this.name = name;
	}

	public void createStaff(Scanner sc) {
		System.out.print("Nhập mã NV: ");
		this.code = sc.nextInt();
		sc.nextLine();
		System.out.print("Nhập tên NV: ");
		this.name = sc.nextLine();
	}
	
	@Override
	public String toString() {
		return "[code=" + code + ", name=" + name + "]";
	}
	

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
