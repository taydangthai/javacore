package service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import model.Staff;

public class StaffServiceImpl implements StaffService {

	List<Staff> listStaff = new ArrayList<Staff>();

	public Scanner sc = new Scanner(System.in);

	public List<Staff> listData() {

		List<Staff> arraylist = new ArrayList<Staff>();
		Staff staff1 = new Staff(1, "tay1");
		Staff staff2 = new Staff(2, "tay2");
		Staff staff3 = new Staff(3, "tay3");
		Staff staff4 = new Staff(4, "tay4");
		Staff staff5 = new Staff(5, "tay5");
		Staff staff6 = new Staff(6, "tay6");

		arraylist = Arrays.asList(staff1, staff2, staff3, staff4, staff5, staff6);

		listStaff.addAll(arraylist);
		return listStaff;
	}

	@Override
	public List<Staff> createStaff() {
		List<Staff> newList = new ArrayList<Staff>();
		int n;
		System.out.print("Nhập số lượng Nhân Viên: ");
		do {
			if (sc.hasNextInt()) {
				n = sc.nextInt();
				for (int i = 0; i < n; i++) {
					System.out.println("nhân viên thứ " + (i + 1));
					Staff staff = new Staff();
					staff.createStaff(sc);
					newList.add(staff);
					listStaff.addAll(newList);
				}
				System.out.println("Thêm thành công!");
				return listStaff;
			} else {
				System.out.print(sc.next() + " K đúng kiểu - nhập lại đy: ");
				/*
				 * System.out.println("\nK đúng Y/c - nhập lại đy: "); return;
				 */
			}
		} while (true);
	}

	@Override
	public List<Staff> displayStaff() {
		for (Staff staff : listStaff) {
			System.out.println("Nhân viên : " + staff.toString());
		}
		return listStaff;
	}

	@Override
	public void removeStaff(int code) {
		for (Staff staff : listStaff) {
			if (staff.getCode() == code) {
				listStaff.remove(staff);
				System.out.println("đã xoá " + code);
				return;
			}
		}
		System.out.println("không tìm thấy");

	}

	@Override
	public void updateStaff(int code, String newName) {
		for (Staff staff : listStaff) {
			if (staff.getCode() == code) { // &&staff.getName().equals(newName)
				staff.setCode(code);
				staff.setName(newName);
				System.out.println("Đã sửa nhân viên có mã: " + code);
				return;
			}
		}
		System.out.println("không tìm thấy");
	}
}
