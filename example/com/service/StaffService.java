package service;

import java.util.List;

import model.Staff;

public interface StaffService {
	public List<Staff> createStaff();
	public List<Staff> displayStaff();
	public void removeStaff(int code);
	public void updateStaff(int code, String newName);
}
