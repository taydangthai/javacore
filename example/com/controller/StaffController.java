package controller;


import java.util.List;
import java.util.Scanner;

import model.Staff;
import service.StaffServiceImpl;

public class StaffController {

	public static void main(String[] args) {

		StaffServiceImpl staffServiceImpl = new StaffServiceImpl();
		List<Staff> list;
		
		while (true) {

			System.out.println("---------------Menu---------------");
			System.out.println("0 - Hiển Thị Danh Sách Mặc Định!");
			System.out.println("1 - Hiển Thị Danh Sách Nhân Viên!");
			System.out.println("2 - Thêm Mới Nhân Viên!");
			System.out.println("3 - Xoá Nhân Viên!");
			System.out.println("4 - Sửa Nhân Viên!");
			System.out.println("----------------------------------");
			System.out.print("Nhập tại đây: ");
			Scanner sc = new Scanner(System.in);
			if (sc.hasNextInt()) {
				int n = sc.nextInt();
				switch (n) {
				case 0:
					list = staffServiceImpl.listData();
				case 1:
					list =  staffServiceImpl.displayStaff();
					break;
				case 2:
					list = staffServiceImpl.createStaff();
					break;
				case 3:
					System.out.print("Nhập mã NV cần xoá: ");
					if (sc.hasNextInt()) {
						int code = sc.nextInt();
						sc.nextLine();
						staffServiceImpl.removeStaff(code);
					} else {
						System.out.println("nhập k đúng!");
						sc.next();
					}
					break;
				case 4:
					System.out.print("Nhập mã NV cần sửa: ");
					int code1 = sc.nextInt();
					sc.nextLine();
					System.out.print("Nhập tên NV cần sửa: ");
					String name = sc.nextLine();
					staffServiceImpl.updateStaff(code1, name);
					break;
				default:
					System.out.println("----------------------------------");
					System.out.println("Không có chương trình như mong muốn - chương trình kết thúc");
					System.exit(0);
					break;
				}
			} else {
				System.out.println(sc.nextLine() + " - Không phải kiểu số, nhập lại! ");
			}

		}
	}
}
