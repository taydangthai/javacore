package com.company.service;

import com.company.controller.StaffController;
import com.company.model.Staff;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StaffService {
    //Khai báo kết nối
    private Connection conn = null;

    public List<Staff> layDanhSach() throws SQLException {
        //Khai báo 1 danh sách
        List<Staff> staffList = new ArrayList();

        try {
            //goi ham ket noi toi mysql
            conn = StaffController.Connect();

            //Khai báo 1 công việc
            String strSQL = "Select code, name"+" from staff";

            Statement comm = conn.createStatement();

            //Thực hiện và trả về kết quả
            ResultSet rs = comm.executeQuery(strSQL);

            //Khai báo đối tượng
            Staff staff = null;
            //Đọc từng dòng thông tin
            while (rs.next()) {
                //Khởi tạo đối tượng
                staff = new Staff();
                //Gán giá trị cho các thuộc tính
                staff.setCode(rs.getInt("code"));
                staff.setName(rs.getString("name"));
                //Thêm vào danh sách
                staffList.add(staff);
            }

        } catch (SQLException ex) {
            System.out.println("Có lỗi xảy ra trong quá trình làm việc với mysql. "
                    + "Chi tiết: " + ex.getMessage());
        } finally {
            try {
                //Đóng kết nối
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(StaffService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return staffList;
    }
    public boolean themMoi(Staff staff) {
        try {

            //Khởi tạo kết nối
            conn = StaffController.Connect();

            //Tạo công việc
            String strInsert = "Insert into staff(code, name) values(?, ?)";
            PreparedStatement comm = conn.prepareStatement(strInsert);

            //Gán giá trị cho các tham số
            comm.setInt(1, staff.getCode());
            comm.setString(2, staff.getName());

//            comm.setDate(8, new Date(objSV.getNgaySinh().getTime()));
            //Thực hiện công việc
            return comm.executeUpdate()>0;

        } catch (SQLException ex) {
            System.err.println("Có lỗi không thêm được thông tin. Chi tiết: " + ex.getMessage());;
        } finally {
            try {
                //Đóng kết nối
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(StaffService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return false;
    }
    public String xoa(int code) {

        try {
            //Khởi tạo kết nối
            conn = StaffController.Connect();

            Statement comm = conn.createStatement();

            //Câu lệnh thực hiện
            String strDelete = "Delete from staff where code = '" + code + "'";
            System.out.println("xoá thành công");
            //Thực hiện xóa và trả về kết quả
            return String.valueOf(comm.executeUpdate(strDelete) > 0);

        } catch (SQLException ex) {
            Logger.getLogger(StaffService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                //Đóng kết nối
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(StaffController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return "ok";
    }
    public boolean capNhat(Staff staff) {

        try {
            //Khởi tạo kết nối
            conn = StaffController.Connect();

            //Tạo công việc
            String strUpdate = "update staff set name = ?  where code = ?; ";
            PreparedStatement comm = conn.prepareStatement(strUpdate);

            //Gán giá trị cho các tham số

            comm.setString(1, staff.getName());
            comm.setInt(2, staff.getCode());

            //Thực hiện công việc

            return comm.executeUpdate()>0;

        } catch (SQLException ex) {
            Logger.getLogger(StaffService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                //Đóng kết nối
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(StaffService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return false;
    }
}
