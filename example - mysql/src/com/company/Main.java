package com.company;

import com.company.model.Staff;
import com.company.service.StaffService;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws SQLException {

        StaffService staffService = new StaffService();
        int paramCode;
        String paramName;
        while (true) {

            System.out.println("---------------Menu---------------");
            System.out.println("1 - Hiển Thị Danh Sách Nhân Viên!");
            System.out.println("2 - Thêm Mới Nhân Viên!");
            System.out.println("3 - Xoá Nhân Viên!");
            System.out.println("4 - Sửa Nhân Viên!");
            System.out.println("----------------------------------");
            System.out.print("Nhập tại đây: ");
            Scanner sc = new Scanner(System.in);
            if (sc.hasNextInt()) {
                int n = sc.nextInt();
                switch (n) {
                    case 1:

                        List<Staff> staffList = staffService.layDanhSach();
                        System.out.println("Đây là Danh Sách Nhân Viên: ");
                        for (Staff staff: staffList) {
                            System.out.println(staff);
                        }
                        break;
                    case 2:
                        System.out.print("Nhập mã NV: ");
                        paramCode = sc.nextInt();
                        sc.nextLine();
                        System.out.print("Nhập tên NV: ");
                        paramName= sc.nextLine();
                        Staff staff = new Staff();
                        staff.setName(paramName);
                        staff.setCode(paramCode);
                        staffService.themMoi(staff);

                        break;
                    case 3:
                        System.out.print("Nhập mã NV cần xoá: ");
                        if (sc.hasNextInt()) {
                            paramCode = sc.nextInt();
                            sc.nextLine();
                            staffService.xoa(paramCode);
                        } else {
                            System.out.println("nhập k đúng!");
                            sc.next();
                        }
                        break;
                    case 4:
                        System.out.print("Nhập mã NV cần sửa: ");
                        paramCode = sc.nextInt();
                        sc.nextLine();
                        System.out.print("Nhập tên NV cần sửa: ");
                        paramName = sc.nextLine();
                        Staff staff1 = new Staff();

                            staff1.setCode(paramCode);
                            staff1.setName(paramName);
                            staffService.capNhat(staff1);
                        break;
                    default:
                        System.out.println("----------------------------------");
                        System.out.println("Không có chương trình như mong muốn - chương trình kết thúc");
                        System.exit(0);
                        break;
                }
            } else {
                System.out.println(sc.nextLine() + " - Không phải kiểu số, nhập lại! ");
            }
        }
    }
}
